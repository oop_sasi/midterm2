/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm2;

/**
 *
 * @author admin
 */
public class Eraser extends Stationery {
    public Eraser(String name, String color, int level) {
        super(name, color, 6, level);
        System.out.println("Eraser Created");
        System.out.println("Eraser name : " + this.name + " color : " + this.color + " numberOfSide : " + 
                this.numberOfSide + " level : " + this.level);
    }
    
    @Override
    public void Write() {
        System.out.println("Stationery : Eraser name : " + this.name + " Can't Write Word");
    }
    
    @Override
    public void Delete() {
        System.out.println("Stationery : Eraser name : " + this.name + " Delete Word");
    }
}
