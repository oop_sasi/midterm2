/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm2;

/**
 *
 * @author admin
 */
public class HighlightPen extends Stationery {
    public HighlightPen(String name, String color, int numberOfSide, int level) {
        super(name, color, numberOfSide, level);
        System.out.println("HighlightPen Created");
        System.out.println("HighlightPen name : " + this.name + " color : " + this.color + " numberOfSide : " + 
                this.numberOfSide + " level : " + this.level);
    }
    
    @Override
    public void Write() {
        System.out.println("Stationery : HighlightPen name : " + this.name + " Highlight Word");
    }
    
    @Override
    public void Delete() {
        System.out.println("Stationery : HighlightPen name : " + this.name + " Can't Delete Word");
    }
}
