/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm2;

/**
 *
 * @author admin
 */
public class MainProgram {
    public static void main(String[] args) {
        Condo condo = new Condo(1,4);
        Stationery stationery1 = new Stationery("stationery1","White",1,1);
        
        stationery1.Write();
        stationery1.Delete();
        
        System.out.println("----------------------");
        
        Pen pen1 = new Pen("pen1","Blue",1,3);
        pen1.Write();
        pen1.Delete();
        
        System.out.println("----------------------");
        
        Pencil pencil1 = new Pencil("pencil1","Black",1,2);
        pencil1.Write();
        pencil1.Delete();
        
        System.out.println("----------------------");
        
        HighlightPen HLpen1 = new HighlightPen("HLpen1","Yellow",2,1);
        HLpen1.Write();
        HLpen1.Delete();
        
        System.out.println("----------------------");
        
        Eraser eraser1 = new Eraser("eraser1","White",1);
        eraser1.Write();
        eraser1.Delete();
        
        System.out.println("----------------------");
        
        CorrectionPen CRpen1 = new CorrectionPen("CRpen1","Blue",4);
        CRpen1.Write();
        CRpen1.Delete();
    }
}
