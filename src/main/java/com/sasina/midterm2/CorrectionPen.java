/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm2;

/**
 *
 * @author admin
 */
public class CorrectionPen extends Stationery{
    public CorrectionPen(String name, String color, int level) {
        super(name, color,1, level);
        System.out.println("CorrectionPen Created");
        System.out.println("CorrectionPen name : " + this.name + " color : " + this.color + " numberOfSide : " + 
                this.numberOfSide + " level : " + this.level);
    }
    
    @Override
    public void Write() {
        System.out.println("Stationery : CorrectionPen name : " + this.name + " Can't Write Word");
    }
    
    @Override
    public void Delete() {
        System.out.println("Stationery : CorrectionPen name : " + this.name + " Delete Word");
    }
}
