/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.midterm2;

/**
 *
 * @author admin
 */
public class Stationery {
    protected String name;
    protected String color;
    protected int numberOfSide;
    protected int level;
    

    public Stationery(String name, String color, int numberOfSide, int level) {
        this.name = name;
        this.color = color;
        this.numberOfSide = numberOfSide;
        this.level = level;
        System.out.println("Stationery Created");
        System.out.println("Stationery name : " + this.name + " color : " + this.color + " numberOfSide : " + 
                this.numberOfSide + " level : " + this.level);
        
    }
    
    public void Write() {
        System.out.println("Stationery : Stationery name : " + this.name + " Write Word");
    }
    
    public void Delete() {
        System.out.println("Stationery : Stationery name : " + this.name + " Delete Word");
    }
    
}
